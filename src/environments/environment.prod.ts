export const environment = {
  production: true,
  HOST:'https://jcvi4byieh.execute-api.us-east-1.amazonaws.com/stage/default',
  URL_API_GATEWAY:'https://jcvi4byieh.execute-api.us-east-1.amazonaws.com/stage'
};
