import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { NewUsers } from '../interfaces/newusers.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private url = `${environment.URL_API_GATEWAY}/auth`;

  constructor(
    private httpClient: HttpClient
  ) { }

   //Metodo para registrar un usuario
   signUp(user: string, email: string, pass: string): Observable<any> {
    // let params: HttpParams = new HttpParams()
    let credentials = {
      auth: 'register',
      name: user,
      email: email,
      password: pass
    }
    return this.httpClient.post(`${this.url}/auth`, credentials);
  }

  registrerUsers(newusers: NewUsers): Observable<NewUsers> {
    return this.httpClient.post<NewUsers>(`${this.url}`, newusers);
  }

  getUsers():Observable<NewUsers[]>{
    return this.httpClient.get<NewUsers[]>(`${this.url}`);
  }

  getUsersById(id: any):Observable<NewUsers>{
    return this.httpClient.get<NewUsers>(`${this.url}/${id}`);
  }
}
