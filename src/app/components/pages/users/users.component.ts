import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NewUsers } from 'src/app/interfaces/newusers.interface';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'uni-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  usersList !: NewUsers[];
  dataSource: any;
  displayedColumns: string[] = ["nameu", "email", "pass"];
  @ViewChild(MatPaginator) paginator !: MatPaginator;
  @ViewChild(MatSort) sort !: MatSort;

  constructor(
    
    private service: UsersService,
    private dialog: MatDialog,
    private _liveAnnouncer: LiveAnnouncer
  ) {
    this.getlist();
  }

  ngOnInit(): void {
  }

  getlist() {
    this.service.getUsers().subscribe(res => {
      this.usersList = res;
      this.dataSource = new MatTableDataSource<NewUsers>(this.usersList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

}
