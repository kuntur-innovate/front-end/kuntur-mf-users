import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'uni-savesettings',
  templateUrl: './savesettings.component.html',
  styleUrls: ['./savesettings.component.scss']
})
export class SavesettingsComponent implements OnInit {

  constructor(private ref:MatDialogRef<SavesettingsComponent>) {}
  closepopup() {
    this.ref.close('Closed using function');
  }

  ngOnInit(): void {
 }

}
