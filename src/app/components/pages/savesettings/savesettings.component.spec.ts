import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SavesettingsComponent } from './savesettings.component';

describe('SavesettingsComponent', () => {
  let component: SavesettingsComponent;
  let fixture: ComponentFixture<SavesettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SavesettingsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SavesettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
