import { DOCUMENT, DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'uni-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.scss'],


})
export class NewuserComponent implements OnInit {

  public newUsers: FormGroup;
  public data;
  public dataNewUsers;
  public buttonClicked: boolean = false;
  public variable;

  constructor(
    private formBuilder: FormBuilder,
    private UsersService: UsersService,
    private datePipe: DatePipe,
    @Inject(DOCUMENT) private document: Document
  ) { }

  // get typeuser() {
  //   return this.formUser.get('typeuser') as FormControl;
  // }

  // get nameu() {
  //   return this.formUser.get('nameu') as FormControl;
  // }

  // get email() {
  //   return this.formUser.get('email') as FormControl;
  // }

  // get pass() {
  //   return this.formUser.get('pass') as FormControl;
  // }

  formUsers = this.formBuilder.group({
    // 'typeuser' : ['', Validators.required],
    'nameu': ['', Validators.required],
    'email': ['', [Validators.required, Validators.email]],
    'pass': ['', Validators.required],
  });

  // procesar(){
  //   console.log(this.formUser.value);
  // }

  ngOnInit() {
    this.newUsers = new FormGroup({
      nameu: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      pass: new FormControl('', Validators.required)
    })
    console.log(new Date().toDateString());
  }

  getDate() {
    return new Date()
  }

  genId(): string {
    const prefix: string = 'USERS-';
    const year: string = this.getDate().getFullYear().toString();
    const randomSuffix: string = this.generateRandomString(5, '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ');

    return `${prefix}${year}-${randomSuffix}`;
  }

  generateRandomString(length: number, characters: string): string {
    let result: string = '';
    const charactersLength: number = characters.length;

    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
  }

  formatDate(date: Date): string {
    return this.datePipe.transform(date, 'dd/MM/yyyy') || '';
  }

  onSubmit() {
    if (this.newUsers.valid) {
      console.log(this.newUsers.value);
      this.dataNewUsers = JSON.stringify(this.newUsers.value);
      this.UsersService.registrerUsers(this.dataNewUsers);
      // Aquí puedes enviar los datos a través del servicio de auth
    } else {
      console.log("Formulario inválido. Por favor, completa todos los campos.");
    }
    this.buttonClicked = true;
    console.log(typeof this.newUsers);
  }

}



