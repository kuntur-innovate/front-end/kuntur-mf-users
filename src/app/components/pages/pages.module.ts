import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { PageRoutingModule } from './pages.routing.module';
import { UsersComponent } from './users/users.component';
import { NewuserComponent } from './newuser/newuser.component';
import { RolsComponent } from './rols/rols.component';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  declarations: [
    UsersComponent,
    NewuserComponent,
    RolsComponent
  ],
  imports: [
    CommonModule, 
    MaterialModule, 
    PageRoutingModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTableModule
  ],
  providers: [
    DatePipe
  ]
})
export class PagesModule { }
