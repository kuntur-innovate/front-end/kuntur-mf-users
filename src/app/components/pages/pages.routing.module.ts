import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { NewuserComponent } from './newuser/newuser.component';
import { RolsComponent } from './rols/rols.component';
import { APP_BASE_HREF } from '@angular/common';

const routes: Routes = [
  // {path:'prueba', component:NombreComponenteComponent},
  { path: 'rols', component: RolsComponent },
  { path: 'newuser', component: NewuserComponent },
  { path: '**', component: UsersComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [{ provide: APP_BASE_HREF, useValue: "/" }],

})
export class PageRoutingModule { }