import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import { SavesettingsComponent } from '../savesettings/savesettings.component';
import {ThemePalette} from '@angular/material/core';


@Component({
  selector: 'uni-rols',
  templateUrl: './rols.component.html',
  styleUrls: ['./rols.component.scss']
})
export class RolsComponent implements OnInit {

  isChecked = true;


  constructor(public dialog: MatDialog) { }

  openDialog() {
    this.dialog.open(SavesettingsComponent);
  }

  ngOnInit(): void {
  }

}
